import React, {Component} from 'react';
import logo from './logo.svg';
import './App.css';

class App extends React.Component{
   
  
  constructor(props){
    super(props)
    this.state = { 
      board : Array(9).fill(null),
      player: "X",
      winner: null
    }
  }

  Winner(){
    let winlines=[
      ['0','1','2'],
      ['3','4','5'],
      ['6','7','8'],
      ['0','3','6'],
      ['1','4','7'],
      ['2','5','8'],
      ['0','4','8'],
      ['2','4','6']
    ]

    for(let i = 0; i<winlines.length;i++){
      const [a,b,c]= winlines[i];
      if(this.state.board[a]&& this.state.board[a]===this.state.board[b] && this.state.board[a]===this.state.board[c]){
        alert(this.state.player + "  won the match")
      }
    }


  }

  Click(index){
    let newboard = this.state.board

    if(this.state.board[index]===null && !this.state.winner){

    newboard[index]= this.state.player
    this.setState({
      board:newboard,
      player: this.state.player==="X"?"O":"X"
    })

    this.Winner()

    }
      }


render(){

  const Box = this.state.board.map((box,index) => <div className = "box" key={index} onClick= {()=> {this.Click(index)}}>{box}</div>)
  return (

    
    <div className= "title">
    <h1>Tic Tac Toe</h1>    

    <div className="board" >
        {Box}  
    </div>
    </div>
  );
}
}
export default App;
